create login Owner12 with password = 'Owner12345', DEFAULT_DATABASE = "shop"
use shop
create user Owner12 for login Owner12;

create role Owner;
GO
GRANT UPDATE ON owners (dateBirth) TO Owner
GRANT SELECT ON ownersShip TO Owner
GRANT UPDATE ON ownersShip TO Owner
GRANT ALTER ON shops TO Owner
GRANT DELETE ON shops TO Owner
GRANT INSERT ON shops TO Owner
GRANT SELECT ON shops TO Owner
GRANT UPDATE ON shops TO Owner
GRANT DELETE ON owners TO Owner
GRANT SELECT ON owners TO Owner
GRANT UPDATE ON owners (addres) TO Owner
GRANT UPDATE ON owners (locationLive) TO Owner
GRANT ALTER ON ageReg TO Owner
GRANT INSERT ON ageReg TO Owner
GRANT SELECT ON ageReg TO Owner
GO

alter role Owner add member Owner12;