exec sp_addlogin @loginame = 'Accountant123',  @passwd='Accountant123', @defdb="shop"
use shop;
exec sp_addrole Accountant

exec sp_adduser @loginame='Accountant123', @grpname='Accountant'

GO
GRANT UPDATE ON ownersShip TO Accountant
GRANT DELETE ON ownersShip TO Accountant
GRANT ALTER ON ownersShip TO Accountant
GRANT SELECT ON ownersShip TO Accountant
GRANT INSERT ON ownersShip TO Accountant
GRANT SELECT ON owners TO Accountant
GRANT SELECT ON shops TO Accountant
GRANT UPDATE ON shops TO Accountant
GRANT ALTER ON ageReg TO Accountant
GRANT INSERT ON ageReg TO Accountant
GRANT SELECT ON ageReg TO Accountant
GO